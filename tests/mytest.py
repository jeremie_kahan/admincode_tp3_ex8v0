#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import logging

from calculator.simplecalculator import SimpleCalculator as SimpleCalculator

class AdditionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        #self.calculator = SimpleCalculator()

    def test_addition_two_integers(self):
        SimpleCalculator("Somme", 2, 3)
        result = SimpleCalculator("Somme", 2, 3).calcul()
        self.assertEqual(result, 5)
        logging.warning('test_addition_two_integer ok warn') #message ca marche
        logging.info('test_addition_two_integer ok inf') #affiche si on active dans les options pytest

    def test_addtion_integer_string(self):
        SimpleCalculator("Somme", 2, "3")
        result = SimpleCalculator("Somme", 2, "3").calcul()
        self.assertEqual(result, "ERROR")

    def test_addition_negative_integers(self):
        SimpleCalculator("Somme", -2, -3)
        result = SimpleCalculator("Somme", -2, -3).calcul()
        self.assertEqual(result, -5)




if __name__ == "__main__":
    unittest.main()
